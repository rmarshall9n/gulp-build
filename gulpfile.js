/**
 * Include plugins
 */
var gulp            = require('gulp')
    plumber         = require('gulp-plumber')
    rename          = require('gulp-rename')
    cache           = require('gulp-cache')
    sourcemaps      = require('gulp-sourcemaps')
    runSequence     = require('run-sequence')
    del             = require('del')

    // js
    uglify          = require('gulp-uglify')

    // scss
    sass            = require('gulp-sass')
    sassLint        = require('gulp-sass-lint')
    postcss         = require('gulp-postcss')
    cssnano         = require('cssnano')
    autoprefixer    = require('autoprefixer')
    mqpacker        = require('css-mqpacker')
    purgecss        = require('gulp-purgecss')

    // images
    imagemin        = require('gulp-imagemin')

    // browser sync
    browserSync     = require('browser-sync').create()
    reload          = browserSync.reload
;

var config = {
    url: 'foo-client.dev',
    paths: {
        app: './app',

        srcScripts: './assets/js',
        srcStyles: './assets/scss',
        srcImages: './assets/images',
        srcFonts: './assets/fonts',

        destScripts: './app/js',
        destStyles: './app/css',
        destImages: './app/images',
        destFonts: './app/fonts',

        // tailwindConfig: './assets/config/tailwind.js',
    },
};

gulp.task('help', function(){
    console.log('==============');
    console.log('  GULP TASKS');
    console.log('==============');
    console.log('');
    console.log('build:all - Build all assets.');
    console.log('build:serve - Start up a browser sync and watch for changes.');
    console.log('build:clean - Remove all built css, js, fonts.');
    console.log('cache:clear - Remove all cahced content e.g. images.');
    console.log('');
    console.log('styles:lint - Check sass files for errors.');
    console.log('styles:compile - Compile, Minify, ');
    console.log('js:compile - Minify and create sourcemaps.');
    console.log('images:optimise - Cache and push through imagemin.');
    console.log('fonts:copy - Copy across all fonts to dest.');
    console.log('bower:copy - Copy all bower assets across to the dest into their correct folders.');
    console.log('html:reload - Force a browser reload on html change.');
    console.log('browser-sync - Initialise browser sync with server or proxy.');
    console.log('watch - Watch for asset changes.');
    console.log('useref - Use to combine assets (not currently in use).');
    console.log('');
});


/**
 * Html
 */
gulp.task('html:reload', function(){
    gulp.src(config.paths.app+'/**/*.html')
    .pipe(reload({stream:true}));
});


/**
 * Scripts
 */
gulp.task('js:compile', function(){

    gulp.src([config.paths.srcScripts+'/**/*.js', '!'+config.paths.srcScripts+'/**/*.min.js'])
        .pipe(plumber())
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(config.paths.destScripts))
        .pipe(reload({stream:true}));
});

/**
 * Copy bower assets across to dist
 * Overides can be found in bower.json
 */
gulp.task('bower:copy', function(){

    var bower = require('main-bower-files');

    gulp.src(
        bower({
            filter: '**/*.js'
        }))
        .pipe(gulp.dest(config.paths.destScripts));

    gulp.src(
        bower({
            filter: '**/*.css'
        }))
        .pipe(gulp.dest(config.paths.destStyles));

    gulp.src(
        bower({
            filter: '**/*.+(eot|svg|ttf|woff|woff2)'
        }))
        .pipe(gulp.dest(config.paths.destFonts));
});


/**
 * SASS
 */
/** Run through linter */
gulp.task('styles:lint', function() {
    gulp.src([
        config.paths.srcStyles+'/**/*.scss',
        // '!'+config.paths.srcStyles+'/base/_normalize.scss'
    ])
    .pipe(sassLint())
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError());
});

/** Compile down to css */
gulp.task('styles:compile', function(){
    // var tailwind = require('tailwindcss');

    return gulp.src(config.paths.srcStyles+'/**/*.scss')
        .pipe(plumber())
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            errLogToConsole: true,
        }))
        .pipe(postcss([
            // tailwindcss(config.paths.tailwindConfig),
            autoprefixer({ browsers: ['last 2 version'] }),
            mqpacker({ sort: true }),
            cssnano({save: true}),
        ]))
        .pipe(purgecss({
            content: [config.paths.app+"/**/*.html"]
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(config.paths.destStyles))
        .pipe(reload({stream:true}));
});

/**
 * Images
 */
gulp.task('images:optimise', function(){
    return gulp.src(config.paths.srcImages+'/**/*.+(png|jpg|gif|svg)')
        .pipe(
            cache(
                imagemin([
                    imagemin.gifsicle({interlaced: true}),
                    imagemin.jpegtran({progressive: true}),
                    imagemin.optipng({optimizationLevel: 5}),
                    imagemin.svgo({
                        plugins: [
                            {removeViewBox: true},
                            {cleanupIDs: false}
                        ]
                    })
                ])
            )
        )
        .pipe(gulp.dest(config.paths.destImages))
        .pipe(reload({stream: true}))
});


/**
 * Fonts
 */
gulp.task('fonts:copy', function() {
    return gulp.src(config.paths.srcFonts+'/**/*')
        .pipe(gulp.dest(config.paths.destFonts))
});


/**
 * Browser Sync
 */
gulp.task('browser-sync', function(){
    browserSync.init({
        server: {
            baseDir: config.paths.app,
            port: 4040
        }
    });
});



gulp.task('useref', function(){
    var useref = require('gulp-useref');

    return gulp.src(config.paths.app+'/*.html')
        .pipe(useref())
        // .pipe(gulpIf('*.js', uglify()))
        // .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest('dist'))
});


/**
 * WATCHER
 */
gulp.task('watch', function(){
    gulp.watch(config.paths.app+'/**/*.html', ['html:reload']);
    gulp.watch(config.paths.srcScripts+'/**/*.js', ['js:compile']);
    gulp.watch(config.paths.srcStyles+'/**/*.scss', ['styles:compile']);
    gulp.watch(config.paths.srcImages+'/**/*', ['images:optimise']);
    gulp.watch(config.paths.fonts+'/**/*', ['fonts']);
});


/**
 * build
 */
gulp.task('build:all', [
    'js:compile',
    'bower:copy',
    'styles:compile',
    'images:optimise',
    'fonts:copy'
]);

gulp.task('build:serve', [
    'browser-sync',
    'watch'
], function() {
    browserSync.reload;
});

gulp.task('build:clean', function(){
    del([
        config.paths.destScripts+'/*',
        config.paths.destStyles+'/*',
        config.paths.destFonts+'/*',
    ]);
});

gulp.task('cache:clear', function(){
    del(config.paths.destImages+'/**/*');
    return cache.clearAll();
});

/**
 * Default task
 */
gulp.task('default', function(callback) {
  runSequence('build:clean', 'build:all', 'build:serve', callback);
});